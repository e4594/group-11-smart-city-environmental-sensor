import pandas as pd # Convention is to import pandas as pd
import sqlite3 as sql # Just to shorten to sql, not a convention AFAIK

db_path = r"/home/pi/database/smart_cities.db"

conn = sql.connect(db_path) # Establish a connection with the database

df = pd.read_sql_query('SELECT * FROM MEASUREMENTS',conn) # Table name must coincide with the one in your DB

print(df.head()) # Prints first 5 rows of dataframe
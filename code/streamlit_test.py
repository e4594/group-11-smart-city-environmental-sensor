import streamlit as st
import pandas as pd
import numpy as np
import sqlite3 as sql # Just to shorten to sql, not a convention AFAIK
import plotly.express as px
import os
st.title('hello')

dirname = os.path.dirname(__file__)
database_location = os.path.join(dirname, 'database/smart_cities.db')

conn = sql.connect(database_location) # Establish a connection with the database

temp = pd.read_sql_query('SELECT Temperature FROM MEASUREMENTS',conn)
humid = pd.read_sql_query('SELECT humidity FROM MEASUREMENTS',conn)
dust = pd.read_sql_query('SELECT Dust FROM MEASUREMENTS',conn)
#light = pd.read_sql_query('SELECT Light FROM MEASUREMENTS',conn)

avg_temp=np.average(temp)
avg_humid=np.average(humid)
avg_dust=np.average(dust)
#avg_light=np.average(light)

df = pd.read_sql_query('SELECT * FROM MEASUREMENTS',conn) # Table name must coincide with the one in your DB
st.write(avg_temp,avg_humid,avg_dust)
st.write(df)


fig = px.line(df, x="DATE",y="TEMPERATURE",title="Temperature")
st.plotly_chart(fig)
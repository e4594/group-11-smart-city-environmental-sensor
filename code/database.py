import sqlite3 #wrapper library close to file based
from datetime import datetime
import pandas as pd

#sql
import os
dirname = os.path.dirname(__file__)
database_location = os.path.join(dirname, 'database/smart_cities.db')

db_conn = sqlite3.connect(database_location)
db_cursor = db_conn.cursor()

def  create_date():
    date = datetime.now()
    date = date.strftime("%Y-%m-%d %H:%M:%S:%f")
    return date

sql ='''CREATE TABLE MEASUREMENTS(
        DATE TEXT PRIMARY KEY,
        TEMPERATURE FLOAT NOT NULL,
        HUMIDITY FLOAT NOT NULL,
        DUST FLOAT NOT NULL
    )'''
    
try:
    db_cursor.execute(sql)
except sqlite3.OperationalError as e:
    print(e)
# def random_variables():
#     date = create_date()
#     temp = round(bme_data.temperature,2)
#     humidity = round(bme_data.humidity,2)
#     dust = random.randint(0,100)
#     co2 = random.randint(0,100)
#     light = random.randint(0,100)
#     decibel = random.randint(0,100)
#     return date,temp, humidity,dust,co2,light,decibel

insert = ('''INSERT INTO MEASUREMENTS(DATE, TEMPERATURE, HUMIDITY, DUST) VALUES (?,?,?,?)''')

def insert_data(values):
    date = (create_date(),)
    values = date + values
    db_cursor.execute(insert, values)
    db_conn.commit()
    db_cursor.execute('SELECT * FROM MEASUREMENTS')
    df = pd.read_sql_query('SELECT * FROM MEASUREMENTS',db_conn)
    print(df)
    db_conn.close()
    

#BME280
import smbus2
import bme280

port = 1
address = 0x77
bus = smbus2.SMBus(port)

calibration_params = bme280.load_calibration_params(bus, address)

def bme280_take_reading():
    data = bme280.sample(bus, address, calibration_params)
    return data.temperature, data.pressure, data.humidity
#end of bme280

#DUST
# from dust_sensor import *
# dust = 0
# def dust_sensor_callback(reading):
#     global dust
#     dust = reading

# dust_sensor.monitor(callback = dust_sensor_callback)


temperature , pressure, humidity = bme280_take_reading()
insert_data((temperature,humidity,3))


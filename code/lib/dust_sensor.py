from MCP3008 import MCP3008
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
from time import sleep

ADC = MCP3008()


# pin setup
LED_PIN = 17  # LED
GPIO.setup(LED_PIN, GPIO.OUT)
VO_PIN = 1 # ADC pin 

# constants
SAMPLING_TIME = 0.00028 # 0.28ms
DELTA_TIME = 0.00004 # 0.4ms
SLEEP_TIME = 0.00968 # 9.68 miliseconds to sleep
VOC = 0.6 # ug/m3
MAX = 0


def calc_volt(val):
    return val / 1023 * 3.3


def calc_density(vo, k=0.5):
    global VOC
    global MAX

    dv = vo - VOC
    if dv < 0:
        dv = 0
        VOC = vo
    density = dv / k * 100
    MAX = max(MAX, density)
    return density


def monitor(sample_size=100, callback=None):
    vals = []
    try:
        GPIO.output(LED_PIN, GPIO.LOW)
        sleep(SAMPLING_TIME)
        adc=ADC.read10bit(VO_PIN)
        vals.append(adc)
        sleep(DELTA_TIME)
        GPIO.output(LED_PIN, GPIO.HIGH)
        sleep(SLEEP_TIME)
        if len(vals) == sample_size:
            avg = sum(vals) / len(vals)
            volt = calc_volt(avg)
            density = calc_density(volt)
            mv = volt * 1000
            '''print(
                "{mv} mV / {density} ug/m3 (Voc={voc}) | Max: {max_} ug/m3".format(  
                    mv=round(mv,2),
                    density=round(density,2),
                    voc=round(VOC,2),
                    max_=round(MAX,2),
                )
            )'''
            vals = []
            if callback: 
                callback(density) 
    except KeyboardInterrupt: 
        GPIO.cleanup()
    except Exception: 
        raise
    finally:
        GPIO.output(LED_PIN, GPIO.LOW)